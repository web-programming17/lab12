import { IsNotEmpty } from 'class-validator';
import { MinLength } from 'class-validator/types/decorator/decorators';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  price: number;
}
