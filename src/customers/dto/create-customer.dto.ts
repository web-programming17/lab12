import { IsNotEmpty } from 'class-validator';
import {
  IsNumber,
  IsPhoneNumber,
  MinLength,
} from 'class-validator/types/decorator/decorators';

export class CreateCustomerDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  @IsNumber()
  age: number;

  @IsNotEmpty()
  @MinLength(10)
  @IsPhoneNumber()
  tel: string;

  @IsNotEmpty()
  gender: string;
}
